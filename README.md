# dd-mud-distributer-state-service

Umfangreicher Distributionsservice. Nimmt Spieler-Eingaben entgegen und verteilt diese je nach aktuellem Status an andere Services. Nimmt Antworten der Services entgegen und gibt diese an den jeweiligen Spieler weiter.

## Technik
Basiert auf Spring-Boot. Nutzt Apache Kafka

## Kafka Messaging
Jeder Service, an den Nachrichten gehen, schickt am Ende ein Suffix mit, um anzuzeigen, welcher Status als nächstes folgt. Dieses Suffix beginnt immer mit einem Semikolon und ist drei Zeichen lang.

Suffices:
* ;rtm --> "Return to menu"
* ;sis --> "Stay in State"