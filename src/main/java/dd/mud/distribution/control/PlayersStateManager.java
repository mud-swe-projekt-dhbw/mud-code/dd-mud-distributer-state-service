package dd.mud.distribution.control;

import dd.mud.distribution.state.GameConfigurationState;
import dd.mud.distribution.state.MainMenuState;
import dd.mud.distribution.state.PlayerState;
import dd.mud.distribution.state.PlayingGameState;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Component;
import dd.mud.distribution.kafka.model.MudKafkaMessage;

import java.util.HashMap;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class PlayersStateManager {

    private final AutowireCapableBeanFactory autowireCapableBeanFactory;

    @Getter
    private HashMap<Long, PlayerState> playerIdStateMap = new HashMap<>();

    public void enterPlayerInput (Long playerId, String input) {
        if (playerIdStateMap.get(playerId) == null) {
            setUpNewPlayer(playerId);
        } else {
            reactToInput(playerId, input);
        }
    }

    public void changeState (Long playerId, PlayerStateEnum newState) {
        switch (newState) {
            case CONFIG:
                GameConfigurationState playerState = new GameConfigurationState();
                autowireCapableBeanFactory.autowireBean(playerState);
                this.playerIdStateMap.replace(playerId, playerState);
                playerState.sendInitialMessage(playerId);
                break;
            case MAIN_MENU:
                MainMenuState mainMenuState = new MainMenuState();
                autowireCapableBeanFactory.autowireBean(mainMenuState);
                this.playerIdStateMap.replace(playerId, mainMenuState);
                mainMenuState.showMainMenu(playerId);
                break;
            case PLAYING:
                PlayingGameState playerStatePlaying = new PlayingGameState();
                autowireCapableBeanFactory.autowireBean(playerStatePlaying);
                this.playerIdStateMap.replace(playerId, playerStatePlaying);
                playerStatePlaying.sendInitialMessage(playerId);
                break;
            default:
                // Should not happen
        }
    }

    public void stopWaitingForAnswer (Long playerId) {
        this.playerIdStateMap.get(playerId).setWaitingForAnswer(false);
    }

    private void setUpNewPlayer(Long playerId) {
        MainMenuState mainMenuState = new MainMenuState();
        autowireCapableBeanFactory.autowireBean(mainMenuState);
        playerIdStateMap.put(playerId, mainMenuState);
        mainMenuState.showMainMenu(playerId);
    }

    private void reactToInput (Long playerId, String input) {
        playerIdStateMap.get(playerId).reactToInputByFlag(MudKafkaMessage.builder()
                .id(playerId)
                .message(input)
                .build()
        );
    }
}
