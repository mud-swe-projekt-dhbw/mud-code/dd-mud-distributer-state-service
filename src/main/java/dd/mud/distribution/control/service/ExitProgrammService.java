package dd.mud.distribution.control.service;

import dd.mud.distribution.control.PlayersStateManager;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ExitProgrammService {

    private final PlayersStateManager playersStateManager;

    public void exitFromGameByPlayerId (Long playerId) {
        this.playersStateManager.getPlayerIdStateMap().remove(playerId);
    }
}
