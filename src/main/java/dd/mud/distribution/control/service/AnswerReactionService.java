package dd.mud.distribution.control.service;

import dd.mud.distribution.control.PlayerStateEnum;
import dd.mud.distribution.control.PlayersStateManager;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class AnswerReactionService {

    // Suffices for changing state
    private static final String RETURN_TO_MENU_SUFFIX = "rtm";
    private static final String CHANGE_TO_CONFIGURATION_SUFFIX = "ctc";
    private static final String CHANGE_TO_PLAYING = "ctp";

    private final PlayersStateManager playersStateManager;
    private final SendAnswerProcessorService sendAnswerProcessorService;

    public void receiveAnswer (Long playerId, String answer) {

        System.out.println("gotten answer: " + answer);
        String suffix;
        String[] split = answer.split("[;]");
        if (split.length != 2) {
            // TODO handle exception
        }
        suffix = split[1];
        sendAnswerProcessorService.sendAnswerToPlayer(playerId, split[0]);
        changeStateAccordingToSuffix(playerId, suffix);
    }

    private void changeStateAccordingToSuffix(Long playerId, String suffix) {
        switch (suffix) {
            case RETURN_TO_MENU_SUFFIX:
                playersStateManager.changeState(playerId, PlayerStateEnum.MAIN_MENU);
                break;
            case CHANGE_TO_CONFIGURATION_SUFFIX:
                playersStateManager.changeState(playerId, PlayerStateEnum.CONFIG);
                break;
            case CHANGE_TO_PLAYING:
                playersStateManager.changeState(playerId, PlayerStateEnum.PLAYING);
                break;
            default:
                // Without a state change it should be possible that the user can send messages again.
                playersStateManager.stopWaitingForAnswer(playerId);
        }
    }
}
