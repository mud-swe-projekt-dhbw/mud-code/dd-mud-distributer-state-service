package dd.mud.distribution.control.service;

import dd.mud.distribution.kafka.model.MudKafkaMessage;
import dd.mud.distribution.kafka.producer.AnswerProducer;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SendAnswerProcessorService {

    private final AnswerProducer answerProducer;

    public void sendAnswerToPlayer (Long playerId, String message) {
        answerProducer.sendAnswerBackToWebsocketService(MudKafkaMessage.builder()
                .id(playerId)
                .message(message)
                .build()
        );
    }

}
