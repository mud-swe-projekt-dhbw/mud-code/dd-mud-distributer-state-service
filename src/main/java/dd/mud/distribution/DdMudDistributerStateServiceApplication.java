package dd.mud.distribution;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DdMudDistributerStateServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DdMudDistributerStateServiceApplication.class, args);
	}

}
