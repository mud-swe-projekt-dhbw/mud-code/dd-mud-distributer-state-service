package dd.mud.distribution.state;

import dd.mud.distribution.control.service.ExitProgrammService;
import dd.mud.distribution.kafka.model.MudKafkaMessage;
import dd.mud.distribution.kafka.producer.MessageDistributorProducer;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class PlayerState {

    private static final String EXIT_COMMAND = "exitGame";

    @Getter
    @Setter
    private boolean waitingForAnswer;

    protected MessageDistributorProducer messageDistributorProducer;
    private ExitProgrammService exitProgrammService;

    public PlayerState () {
        this.waitingForAnswer = false;
    }

    public void reactToInputByFlag (MudKafkaMessage mudKafkaMessage) {
        if (waitingForAnswer) {
            doNothing();
        } else if (EXIT_COMMAND.equals(mudKafkaMessage.getMessage())) {
          this.exitProgrammService.exitFromGameByPlayerId(mudKafkaMessage.getId());
        } else {
            this.waitingForAnswer = true;
            reactToPlayerInputByState(mudKafkaMessage);
        }
    }

    protected abstract void reactToPlayerInputByState(MudKafkaMessage mudKafkaMessage);

    public abstract void sendInitialMessage (Long playerId);

    private void doNothing () {
        // Do nothing
    }

    @Autowired
    public void setMessageDistributorProducer(MessageDistributorProducer messageDistributorProducer) {
        this.messageDistributorProducer = messageDistributorProducer;
    }

    @Autowired
    public void setExitProgrammService (ExitProgrammService exitProgrammService) {
        this.exitProgrammService = exitProgrammService;
    }
}
