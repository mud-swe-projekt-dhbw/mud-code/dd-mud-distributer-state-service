package dd.mud.distribution.state;

import dd.mud.distribution.control.service.AnswerReactionService;
import dd.mud.distribution.kafka.model.MudKafkaMessage;
import dd.mud.distribution.state.strings.MainMenuStringConstants;
import org.springframework.beans.factory.annotation.Autowired;

public class MainMenuState extends PlayerState {

    private AnswerReactionService answerReactionService;

    public MainMenuState() {
        super();
    }

    @Override
    protected void reactToPlayerInputByState(MudKafkaMessage mudKafkaMessage) {
        switch (mudKafkaMessage.getMessage()) {
            case "1":
                answerReactionService.receiveAnswer(mudKafkaMessage.getId(), MainMenuStringConstants.START_CONIFG_GAME);
                break;
            case "2":
                answerReactionService.receiveAnswer(mudKafkaMessage.getId(), MainMenuStringConstants.START_PLAYING_GAME);
                break;
            default:
                answerReactionService.receiveAnswer(mudKafkaMessage.getId(), MainMenuStringConstants.FAILURE);
        }
    }

    @Override
    public void sendInitialMessage(Long playerId) {
        // do nothing
    }

    public void showMainMenu(Long playerId) {
        answerReactionService.receiveAnswer(playerId,
                MainMenuStringConstants.HEADER +
                MainMenuStringConstants.HEAD_LINE +
                MainMenuStringConstants.OPTION_ONE_CONFIG_GAME +
                MainMenuStringConstants.OPTION_TWO_ENTER_GAME +
                ";sis"
        );
    }

    @Autowired
    public void setAnswerReactionService (AnswerReactionService answerReactionService) {
        this.answerReactionService = answerReactionService;
    }
}
