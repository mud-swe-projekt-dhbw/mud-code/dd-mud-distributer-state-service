package dd.mud.distribution.state.strings;

public final class MainMenuStringConstants {

    public static final String HEADER ="\n" +
            "██████╗░██████╗░  ███╗░░░███╗██╗░░░██╗██████╗░\n" +
            "██╔══██╗██╔══██╗  ████╗░████║██║░░░██║██╔══██╗\n" +
            "██║░░██║██║░░██║  ██╔████╔██║██║░░░██║██║░░██║\n" +
            "██║░░██║██║░░██║  ██║╚██╔╝██║██║░░░██║██║░░██║\n" +
            "██████╔╝██████╔╝  ██║░╚═╝░██║╚██████╔╝██████╔╝\n" +
            "╚═════╝░╚═════╝░  ╚═╝░░░░░╚═╝░╚═════╝░╚═════╝░\n\n";

    public static final String HEAD_LINE = "Willkommen im MUD: Wähle eine Option (\"1\", \"2\")\n";
    public static final String OPTION_ONE_CONFIG_GAME = "1. Konfiguriere ein neues Spiel\n";
    public static final String OPTION_TWO_ENTER_GAME = "2. Trete einem Spiel bei.\n";

    public static final String START_CONIFG_GAME = "Starte: Spiel konfigurieren...;ctc";
    public static final String START_PLAYING_GAME = "Starte: Spiel beitreten...;ctp";

    public static final String FAILURE = "Falsche eingabe, versuche es noch mal;sis";
}
