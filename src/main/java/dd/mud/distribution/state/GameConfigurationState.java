package dd.mud.distribution.state;

import dd.mud.distribution.kafka.model.MudKafkaMessage;

public class GameConfigurationState extends PlayerState {

    private static final String KAFKA_TOPIC_NAME = "configSend.t";

    public GameConfigurationState() {
        super();
    }

    @Override
    protected void reactToPlayerInputByState(MudKafkaMessage mudKafkaMessage) {
        this.messageDistributorProducer.sendToDistribution(mudKafkaMessage, KAFKA_TOPIC_NAME);
    }

    @Override
    public void sendInitialMessage(Long playerId) {
        this.messageDistributorProducer.sendToDistribution(MudKafkaMessage.builder().id(playerId).message("").build(),
                KAFKA_TOPIC_NAME);
    }
}
