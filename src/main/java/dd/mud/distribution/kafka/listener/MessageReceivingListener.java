package dd.mud.distribution.kafka.listener;

import dd.mud.distribution.control.PlayersStateManager;
import dd.mud.distribution.kafka.model.MudKafkaMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class MessageReceivingListener {

    private final PlayersStateManager playersStateManager;

    @KafkaListener(topics = "playerInput.t", groupId = "distributor-consumer-input")
    public void listenToIncomingMessaging (MudKafkaMessage message) {
        System.out.println("incoming: " + message.getMessage());
        playersStateManager.enterPlayerInput(message.getId(), message.getMessage());
    }
}
