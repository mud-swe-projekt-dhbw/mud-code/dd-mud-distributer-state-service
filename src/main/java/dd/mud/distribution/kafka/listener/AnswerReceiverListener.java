package dd.mud.distribution.kafka.listener;

import dd.mud.distribution.control.service.AnswerReactionService;
import dd.mud.distribution.kafka.model.MudKafkaMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class AnswerReceiverListener {

    private final AnswerReactionService answerReactionService;

    @KafkaListener(topics = {"configAnswer.t", "playAnswer.t"}, groupId = "distributor-consumer")
    public void listenToAnswer (MudKafkaMessage mudKafkaMessage) {
        System.out.println(mudKafkaMessage.getMessage());
        answerReactionService.receiveAnswer(mudKafkaMessage.getId(), mudKafkaMessage.getMessage());
    }
}
