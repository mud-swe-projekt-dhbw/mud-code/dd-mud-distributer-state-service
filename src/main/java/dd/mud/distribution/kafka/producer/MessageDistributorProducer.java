package dd.mud.distribution.kafka.producer;

import dd.mud.distribution.kafka.model.MudKafkaMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class MessageDistributorProducer {

    private final KafkaTemplate<String, MudKafkaMessage> kafkaMessageKafkaTemplate;

    public void sendToDistribution (MudKafkaMessage mudKafkaMessage, String topic) {
        System.out.println("outgoing to topic: " + topic + " message: " + mudKafkaMessage.getMessage());
        kafkaMessageKafkaTemplate.send(topic, mudKafkaMessage);
    }
}
