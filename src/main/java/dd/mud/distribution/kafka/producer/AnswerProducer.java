package dd.mud.distribution.kafka.producer;

import dd.mud.distribution.kafka.model.MudKafkaMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class AnswerProducer {

    private static final String SEND_BACK_TOPIC = "sendToWebsocket.t";

    private final KafkaTemplate<String, MudKafkaMessage> kafkaMessageKafkaTemplate;

    public void sendAnswerBackToWebsocketService (MudKafkaMessage mudKafkaMessage) {
        System.out.println("outgoing: " + mudKafkaMessage.getMessage());
        kafkaMessageKafkaTemplate.send(SEND_BACK_TOPIC, mudKafkaMessage);
    }
}
